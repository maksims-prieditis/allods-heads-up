dofile('./Scripts/Core/Extensions.lua');

local a = {
	name = 'Maksims Iakushin',
	age = 33,
	employment = {
		company = 'Luxoft',
		position = 'Senior Developer'
	}
};

local b = {
	age = 34,
	employment = {
		position = 'Principal Developer',
		onsite = 'Credit Suisse'
	}
};

local c = table.extend(a, b);

assert(c ~= a);
assert(c ~= b);

assert(c.name == 'Maksims Iakushin');
assert(c.age == 34);
assert(c.employment);
assert(c.employment.company == 'Luxoft');
assert(c.employment.position == 'Principal Developer');
assert(c.employment.onsite == 'Credit Suisse');

local d = table.extend(a.employment);

assert(d ~= a.employment);
assert(d.company == 'Luxoft');
assert(d.position == 'Senior Developer');


local items = {};

print(table.insert(items, 1, 5));
print(table.insert(items, 1, 2));
print(table.insert(items, 1, 3));
print(table.insert(items, 1, 6));


table.each(items, function(a, b)
	print(a, b);
end);

table.remove(items, -1);
print('----');
table.each(items, function(a, b)
	print(a, b);
end);