local hooks = Hooks();
print('creating panels');
local ribbonPanels = {
	RibbonPanelUiElement(-250, 0),
	RibbonPanelUiElement(250, 0)
}
print('set layouts');
ribbonPanels[1].layoutStyle(AW_RIBBON_LAYOUT_FIXED);
ribbonPanels[2].layoutStyle(AW_RIBBON_LAYOUT_POOL);

print('creating ribbons');
for i = 1,2 do
	ribbonPanels[i].updateRibbon(1, {timeStyle = AW_TIME_TEXT, name = tostring(1)});
	ribbonPanels[i].updateRibbon(2, {timeStyle = AW_TIME_TEXT, name = tostring(2)});
	ribbonPanels[i].updateRibbon(3, {timeStyle = AW_TIME_TEXT, name = tostring(3)});
	ribbonPanels[i].updateRibbon(4, {timeStyle = AW_TIME_TEXT, name = tostring(4)});
	ribbonPanels[i].updateRibbon(5, {timeStyle = AW_TIME_TEXT, name = tostring(5)});
	ribbonPanels[i].updateRibbon(6, {timeStyle = AW_TIME_TEXT, name = tostring(6)});
	ribbonPanels[i].updateRibbon(7, {timeStyle = AW_TIME_TEXT, name = tostring(7)});
	ribbonPanels[i].updateRibbon(8, {timeStyle = AW_TIME_TEXT, name = tostring(8)});
	ribbonPanels[i].updateRibbon(9, {timeStyle = AW_TIME_TEXT, name = tostring(9)});
	ribbonPanels[i].updateRibbon(10, {timeStyle = AW_TIME_TEXT, name = tostring(10)});
end;

--[[


ribbonPanel.runRibbon(1, 2000, 2000);
ribbonPanel.runRibbon(2, 150000, 150000);
ribbonPanel.runRibbon(3, 150000, 150000);
ribbonPanel.runRibbon(4, 150000, 150000);
ribbonPanel.runRibbon(5, 150000, 150000);
ribbonPanel.runRibbon(6, 150000, 150000);
]]--

function onConfigureHeadsUp(e)
	local position = math.random(10);
	local duration = math.random(25000);
	
	print('run ribbons');
	for i = 1,2 do
		ribbonPanels[i].runRibbon(position, duration, duration);
	end;
end;

hooks.bind('REACTION_CONFIGURE_HEADS_UP', onConfigureHeadsUp);
hooks.enabled(true);
 

--ribbonPanel.updateRibbon(2, {timeStyle = AW_TIME_TEXT});
--ribbonPanel.updateRibbon(3, {timeStyle = AW_TIME_FRACTIONAL});

--ribbonPanel.locked(false);

--ribbonPanel.runRibbon(2, 5000, 5000);
--ribbonPanel.runRibbon(3, 5000, 5000);
