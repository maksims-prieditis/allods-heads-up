Global('AW_UNIT_SELF', 1);
Global('AW_UNIT_TARGET', 2);
Global('AW_UNIT_FOCUS', 4);
Global('AW_UNIT_BOSS', 8);
Global('AW_UNIT_ENEMY', 16);
Global('AW_UNIT_FRIEND', 32);
Global('AW_UNIT_GROUP', 64);
Global('AW_UNIT_RAID', 128);
Global('AW_UNIT_PET', 256);
Global('AW_UNIT_NAME', 512);

Global('AW_MODE_SPELL', 1);
Global('AW_MODE_AURA', 2);
Global('AW_MODE_ITEM', 3);
Global('AW_MODE_SPAWN', 4);
Global('AW_MODE_COOLDOWN', 5);

Global('AW_TIME_NORMAL', 0);
Global('AW_TIME_TEXT', 1);
Global('AW_TIME_FRACTIONAL', 2);

Global('AW_RIBBON_LAYOUT_FIXED', 0);
Global('AW_RIBBON_LAYOUT_POOL', 1);

Global('AW_ALIGNMENT_TOP', 0);
Global('AW_ALIGNMENT_BOTTOM', 1);

Global('AW_DRAG_AXIS_X', 1);
Global('AW_DRAG_AXIS_Y', 2);

Global('AW_SNAP_TO_GRID_WIDTH', 25);
Global('AW_SNAP_TO_GRID_HEIGHT', 25);