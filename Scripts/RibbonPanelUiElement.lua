Define('RibbonPanelUiElement', function(offsetX, offsetY)
	local MARGIN = 5;
	local RIBBON_HEIGHT = 26;

	local _id;
	local _hooks = Hooks();
	local _enabled = false;
	local _locked = true;
	local _container;
	local _ribbons = {};
	local _mapping = {};
	local _options = {};
	local _runtimeState = {};
	local _layout = {};
	local _layoutStyle = 0;

	local function onRuntimeStateUpdated(e)
		if e and e.index then
			local runtimeState = _runtimeState[e.index];
			
			if runtimeState then
				local now = common.GetMsFromDateTime(common.GetLocalDateTime());
				local remaining = runtimeState.timeStamp - now;
				
				_ribbons[e.index]
					:GetChildChecked('Duration', false)
					:SetVal('Content', string.formatTime(remaining, _options[e.index].timeStyle));
			end;
		else
			table.each(_options, function(options, index)
				onRuntimeStateUpdated({index = index});
			end);
		end;	
	end;

	local function onPlayCompleted(e)
		if e.wtOwner then
			local id = e.wtOwner:GetInstanceId();
			
			if _id == id then
				onRuntimeStateUpdated();
				
				if table.any(_runtimeState) then
					_container:PlayFadeEffect(1.0, 1.0, 100, EA_MONOTONOUS_INCREASE);
				end;
			else
				if _mapping[id] then
					local name = e.wtOwner:GetName();
					
					if name == 'Surface' then
						this.resetRibbon(_mapping[id]);
						return;
					end;
				end;
			end;
		end;
	end;

	local function onLayoutChanged()
		local maximum = table.count(_ribbons);

		_container:SetPlacementPlain(
			table.extend(_container:GetPlacementPlain(), { sizeY = MARGIN + maximum * RIBBON_HEIGHT + MARGIN }));
	end;

	local function onRibbonsChanged(index)
		print('onRibbonsChanged(%s)', index);
		
		local maximum = table.count(_ribbons);

		if index then
			--Preparing ribbon
			local ribbon = _ribbons[index];
			local position = index - 1;
			
			if _layoutStyle == AW_RIBBON_LAYOUT_POOL then
				table.remove(_layout, table.index(_layout, index));
				table.insert(_layout, index);
				position = 0;
			end;

			ribbon:SetPlacementPlain(
				table.extend(ribbon:GetPlacementPlain(), { posY = MARGIN + position * RIBBON_HEIGHT }));
		end;

		table.each(_ribbons, function(ribbon, index)
			local position = index - 1;

			if _layoutStyle == AW_RIBBON_LAYOUT_POOL then
				position = maximum - table.index(_layout, index);
			end;
			
			ribbon:PlayMoveEffect(ribbon:GetPlacementPlain(),
						table.extend(ribbon:GetPlacementPlain(), { posY = MARGIN + position * RIBBON_HEIGHT }), 300, EA_MONOTONOUS_INCREASE);
		end);
	end;

	local function onLockedChanged()
		table.each(_ribbons, function(ribbon, index)
			this.resetRibbon(index);
			ribbon:Show(not _locked);
		end);

		if _locked then
			_container:SetBackgroundColor({a = 0, r = 0, g = 0, b = 0});
		else
			_container:SetBackgroundColor({a = 0.5, r = 0, g = 0, b = 0});
		end;
	end;

	function locked(...)
		if passed(...) then
			_locked = ...;
			onLockedChanged();
		end;
		return _locked;
	end;

	local function ensureRibbon(index, options)
		_options[index] = table.extend(_options[index] or {
			background = {r = 1.0, g = 1.0, b = 1.0, a = 1.0}
		}, options);
		
		if _ribbons[index] then
			return _ribbons[index];
		end;
		
		local newRibbon = mainForm:CreateWidgetByDesc(
			_container:GetChildChecked('Ribbon', false):GetWidgetDesc());
		
		_container:AddChild(newRibbon);

		_mapping[newRibbon:GetChildChecked('Surface', false):GetInstanceId()] = index;
		_mapping[newRibbon:GetChildChecked('Blinking', false):GetInstanceId()] = index;

		_ribbons[index] = newRibbon;

		onLayoutChanged();

		return newRibbon;
	end;

	function isBusy(index)
		if index then
			return _runtimeState[index] ~= nil;
		else
			return table.any(_runtimeState);
		end;
	end;

	function layoutStyle(...)
		if passed(...) then
			_layoutStyle = ...;
			onLayoutChanged();
			onRibbonsChanged();
		end;
		return _layoutStyle;
	end;

	function updateRibbon(index, options)
		assert(index > 0);
		
		local ribbon = ensureRibbon(index, options);
		
		if options.background then
			local background = table.extend({r = 1.0, g = 1.0, b = 1.0, a = 1.0}, options.background);
			
			ribbon
				:GetChildChecked('Surface', false)
				:SetBackgroundColor(background);
			ribbon
				:GetChildChecked('Glow', true)
				:SetBackgroundColor(background);
			ribbon
				:GetChildChecked('Blinking', true)
				:SetBackgroundColor(background);
		end;

		if options.enabled ~= nil then
			local background = ribbon
								:GetChildChecked('Surface', false)
								:GetBackgroundColor();
			
			if (options.enabled) then
				background = table.copy(background, {a = 1});
			else
				background = table.copy(background, {a = .5});
			end;
			
			ribbon
				:GetChildChecked('Surface', false)
				:SetBackgroundColor(background);
		end;
		
		if options.name then
			ribbon
				:GetChildChecked('Name', false)
				:SetVal('Content', options.name);
		end;
	end;

	function runRibbon(index, duration, remaining)
		if _locked == false then
			return;
		end;
		
		assert(index > 0);
		
		local ribbon = _ribbons[index];

		if ribbon == nil then
			return;
		end;

		local itemSurface = ribbon:GetChildChecked('Surface', false);
		
		itemSurface:FinishMoveEffect();
		
		if remaining > duration then
			duration = remaining;
		end;
		
		local bounds = _container:GetPlacementPlain();
		local from = itemSurface:GetPlacementPlain();
		local to = table.extend(from, {highPosX = bounds.sizeX - 12});
		
		from.highPosX = to.highPosX - to.highPosX / duration * remaining;
		
		itemSurface:GetChildChecked('Glow', false):Show(true);

		if table.empty(_runtimeState) then --If it's not already playing...
			_container:PlayFadeEffect(1.0, 1.0, 100, EA_MONOTONOUS_INCREASE);
		end;
		
		local now = common.GetMsFromDateTime(common.GetLocalDateTime());
		
		_runtimeState[index] = table.extend(_runtimeState[index], {
			timeStamp = now + remaining;
		});
		
		onRuntimeStateUpdated({index = index});

		onRibbonsChanged(index);
		
		ribbon:Show(true);
		
		itemSurface:PlayMoveEffect(from, to, remaining, EA_MONOTONOUS_INCREASE);
	end;
	
	function resetRibbon(index)
		assert(index > 0);
		
		local ribbon = _ribbons[index];

		if ribbon then
			ribbon:Show(false);

			table.remove(_layout, table.index(_layout, index));

			_runtimeState[index] = nil;
			
			onRibbonsChanged();
			
			local itemSurface = ribbon:GetChildChecked('Surface', false);
			
			itemSurface:FinishMoveEffect();
			itemSurface:SetPlacementPlain(table.extend(itemSurface:GetPlacementPlain(), {highPosX = 1}));
			itemSurface:GetChildChecked('Glow', false):Show(false);
			
			ribbon:GetChildChecked('Blinking', false):Show(false);
			
			ribbon
					:GetChildChecked('Name', false)
					:SetVal('Stacks', '');
		end;	
	end;

	_hooks.bind('EVENT_POS_CONVERTER_CHANGED', onLayoutChanged)
	_hooks.bind('EVENT_EFFECT_FINISHED', onPlayCompleted);
	
	_container = mainForm:CreateWidgetByDesc(
		mainForm:GetChildChecked('Panel', false):GetWidgetDesc());
	
	_id = _container:GetInstanceId();

	_container:SetBackgroundColor({a = .5, r = 0, g = 0, b = 0});
	_container:SetPlacementPlain(
		table.extend(_container:GetPlacementPlain(), { posX = offsetX, posY = offsetY }));
	
	_hooks.enabled(true);
	
	_container:Show(true);
end);
