rawset(_G, 'print', function(message, ...)
	if common.IsWString(message) then
		message = userMods.FromWString(message);
	end;
	
	local arguments = {...};
	
	for i = 1, table.count(arguments) do
		if common.IsWString(arguments[i]) then
			arguments[i] = userMods.FromWString(arguments[i]);
		else
			arguments[i] = tostring(arguments[i]);
		end;
	end;
	
	local now = common.GetLocalDateTime();
	
	local parts = {};
	
	table.insert(parts, string.format('[%02d:%02d:%02d]', now.h, now.min, now.s));
	table.insert(parts, string.format(tostring(message), ...));
	
	common.LogInfo(common.GetAddonName(), table.concat(parts, ' '));
end);
