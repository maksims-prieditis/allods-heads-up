rawset(_G, 'passed', function(...)
	return select('#', ...) > 0;
end);

rawset(table, 'count', function(x)
	local count = 0;
	table.each(x, function()
		count = count + 1;
	end);
	return count;
end);

rawset(table, 'any', function(x)
	if x then
		for element in pairs(x) do
			return true;
		end;
	end;
	return false;
end);

rawset(table, 'empty', function(x)
	return not table.any(x);
end);

rawset(table, 'each', function(x, callback)
	if type(x) == 'table' then
		for key, value in pairs(x) do
			if callback(value, key) then
				return;
			end;
		end;
	end;
end);

rawset(table, 'index', function(a, b)
	if type(a) == 'table' then
		for key, value in pairs(a) do
			if value == b then
				return key;
			end;
		end;
	end;
	return -1;
end);

rawset(table, 'extend', function(a, b)
	local c = {};
	
	if a then
		table.each(a, function(value, name)
			if type(value) == 'table' then
				c[name] = table.extend({}, a[name])
			else
				c[name] = a[name];
			end;
		end);
	end;
	
	if b then
		table.each(b, function(value, name)
			if type(value) == 'table' then
				c[name] = table.extend(a[name], b[name])
			else
				c[name] = b[name];
			end;
		end);
	end;
	
	return c;
end);

if not math.mod then
	rawset(math, 'mod', function(a, b)
		return a - math.floor(a / b) * b;
	end);
end;


rawset(string, 'formatTime', function(milliseconds, mode)
	mode = mode or 0;
	
	local fractional = milliseconds / 1000;
	local minutes = math.floor((fractional + 0.5) / 60)
	local seconds = math.ceil(fractional - minutes * 60)
	
	if (mode == 1) then
		if (minutes > 0) then
			return string.format('%dm', minutes);
		else
			return string.format('%ds', seconds);
		end;
	end;
	
	if (mode == 2) then
		return string.format('%s', math.floor(fractional * 10) / 10);
	end;
	
	return string.format("%d:%02d", minutes, seconds);
end);