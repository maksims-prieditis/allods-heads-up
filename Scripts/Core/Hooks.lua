Define('Hooks', function()
	local _hooks = {};
	local _enabled = false;
	
	function bind(topic, callback)
		if _hooks[topic] then
			this.unbind(topic);
		end;
		
		_hooks[topic] = Hook(topic, callback);
		_hooks[topic].enabled(_enabled);
	end;
	
	function unbind(topic)
		if _hooks[topic] then
			_hooks[topic].enabled(false);
			_hooks[topic] = nil;
		end;
	end;
	
	function enabled(...)
		if passed(...) then
			_enabled = ...;

			table.each(_hooks, function(hook)
				hook.enabled(_enabled);
			end);
		end;
		return _enabled;
	end;
end);

Define('Hook', function(topic, callback)
	local _topic = string.upper(topic);
	local _callback = callback;
	local _enabled = false;
	
	local function onStatusChanged()
		if _topic:match('^REACTION_') then
			if _enabled then
				common.RegisterReactionHandler(_callback, _topic);
				print('+%s', _topic);
			else
				common.UnRegisterReactionHandler(_callback, _topic);
				print('-%s', _topic);
			end;
		else
			if _enabled then
				common.RegisterEventHandler(_callback, _topic);
				print('+%s', _topic);
			else
				common.UnRegisterEventHandler(_callback, _topic);
				print('-%s', _topic);
			end;
		end;
	end;

	function enabled(...)
		if passed(...) then
			if _enabled ~= ... then
				_enabled = ...;
				onStatusChanged();
			end;
		end;
		return _enabled;
	end;
end);
